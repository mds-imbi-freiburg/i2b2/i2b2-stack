# i2b2 Core Server

Docker buildfile to compile the i2b2-core-server code and deploy the application and its configuration files to a wildfly application server. This Setup will work inside the Docker i2b2-net network together with i2b2-web (web-frontend) and i2b2-pg (demo data-backend) container. 

## Quickstart
* Clone repo including its submodules:
  ```
  git clone --recurse-submodules REPOSITORY
  ```
  If you already checked out the repo, but without `recurse-submodules` use following command to get submodules:
  ```
  git submodule update --init --recursive
  ```
* Compile i2b2 core-server and build the wildfly application server image:
  ```
  docker build -t CONTAINER_REGISTRY/i2b2-wildfly .
  ```
* Start i2b2-wildfly:
  If not already done, create the network first:
  ```
  docker network create i2b2-net
  ```
  Run the i2b2 wildfly container
  ```
  docker run -d -p 8080:8080 -p 9090:9090 -p 9009:9009 --net i2b2-net --name i2b2-wildfly CONTAINER_REGISTRY/i2b2-wildfly
  ```

## Info
current versions:
* wildfly 20.0.1
* axis2 1.7.9
* jdbc postgresql 42.2.8
