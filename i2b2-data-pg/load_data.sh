#!/bin/bash

#sleep 9

#export PGPASSWORD=;
psql -h localhost -v ON_ERROR_STOP=1 -U postgres <<-EOSQL
CREATE DATABASE i2b2;
CREATE USER i2b2 WITH SUPERUSER ENCRYPTED PASSWORD 'demouser';
GRANT ALL PRIVILEGES ON DATABASE i2b2 TO i2b2;
EOSQL


USERT=" -d i2b2 -h localhost ";
echo "USERT=$USERT"

#set postgresql i2b2* users password 
#export PGPASSWORD='demouser'


psql -h localhost -v ON_ERROR_STOP=1 -U postgres <<-EOSQL
\c i2b2;
create schema i2b2demodata;
create schema i2b2hive;
create schema i2b2imdata;
create schema i2b2metadata;
create schema i2b2pm;
create schema i2b2workdata;

create user i2b2demodata password 'demouser';
create user i2b2hive password 'demouser';
create user i2b2imdata password 'demouser';
create user i2b2metadata password 'demouser';
create user i2b2pm password 'demouser';
create user i2b2workdata password 'demouser';

GRANT ALL ON SCHEMA i2b2demodata TO i2b2demodata;
GRANT ALL ON SCHEMA i2b2hive TO i2b2hive;
GRANT ALL ON SCHEMA i2b2hive TO i2b2imdata;
GRANT ALL ON SCHEMA i2b2imdata TO i2b2imdata;
GRANT ALL ON SCHEMA i2b2metadata TO i2b2metadata;
GRANT ALL ON SCHEMA i2b2pm TO i2b2pm;
GRANT ALL ON SCHEMA i2b2workdata TO i2b2workdata;

GRANT ALL ON ALL TABLES IN SCHEMA i2b2demodata TO i2b2demodata;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2hive TO i2b2hive;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2hive TO i2b2imdata;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2imdata TO i2b2imdata;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2metadata TO i2b2metadata;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2pm TO i2b2pm;
GRANT ALL ON ALL TABLES IN SCHEMA i2b2workdata TO i2b2workdata;
EOSQL




apt-get update
apt-get install -y ant openjdk-11-jdk


cd /data/NewInstall/Crcdata/

echo "db.type=postgresql
db.username=i2b2demodata
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2demodata
db.project=demo" > db.properties


ant -f data_build.xml create_crcdata_tables_release_1-7
ant -f data_build.xml create_procedures_release_1-7
ant -f data_build.xml db_demodata_load_data



cd ../Hivedata/

echo "db.type=postgresql
db.username=i2b2hive
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2hive" > db.properties

ant -f data_build.xml create_hivedata_tables_release_1-7
ant -f data_build.xml db_hivedata_load_data


cd ../Imdata/

echo "db.type=postgresql
db.username=i2b2imdata
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2imdata
db.project=demo" > db.properties

ant -f data_build.xml create_imdata_tables_release_1-7
ant -f data_build.xml db_imdata_load_data


cd ../Metadata/
 
echo "db.type=postgresql
db.username=i2b2metadata
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2metadata
db.project=demo" > db.properties

ant -f data_build.xml create_metadata_tables_release_1-7
ant -f data_build.xml db_metadata_load_data


cd ../Pmdata

echo "db.type=postgresql
db.username=i2b2pm
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2pm
db.project=demo" > db.properties

ant -f data_build.xml create_pmdata_tables_release_1-7
ant -f data_build.xml create_triggers_release_1-7
ant -f data_build.xml db_pmdata_load_data


cd ../Workdata

echo "db.type=postgresql
db.username=i2b2workdata
db.password=demouser
db.driver=org.postgresql.Driver
db.url=jdbc:postgresql://localhost/i2b2?searchpath=i2b2workdata
db.project=demo" > db.properties

ant -f data_build.xml create_workdata_tables_release_1-7
ant -f data_build.xml db_workdata_load_data



psql -h localhost -v ON_ERROR_STOP=1 -U postgres <<-EOSQL
\c i2b2;
UPDATE i2b2hive.work_db_lookup SET c_db_fullschema = 'i2b2workdata';
UPDATE i2b2hive.im_db_lookup SET c_db_fullschema = 'i2b2imdata';
UPDATE i2b2hive.crc_db_lookup SET c_db_fullschema = 'i2b2demodata';
UPDATE i2b2hive.ont_db_lookup SET c_db_fullschema = 'i2b2metadata';

UPDATE i2b2pm.pm_cell_data SET url = 'http://localhost/i2b2/services/QueryToolService/' WHERE cell_id = 'CRC';
UPDATE i2b2pm.pm_cell_data SET url = 'http://localhost/i2b2/services/IMService/' WHERE cell_id = 'IM';
UPDATE i2b2pm.pm_cell_data SET url = 'http://localhost/i2b2/services/WorkplaceService/' WHERE cell_id = 'WORK';
UPDATE i2b2pm.pm_cell_data SET url = 'http://localhost/i2b2/services/OntologyService/' WHERE cell_id = 'ONT';
UPDATE i2b2pm.pm_cell_data SET url = 'http://localhost/i2b2/services/FRService/' WHERE cell_id = 'FRC';
EOSQL

