-- Deletes the user <ldapname>, its user params and project affiliations
DELETE FROM i2b2pm.pm_project_user_roles WHERE user_id=:'ldapuser';
DELETE FROM i2b2pm.pm_user_params WHERE user_id=:'ldapuser';
DELETE FROM i2b2pm.pm_user_data WHERE user_id=:'ldapuser';