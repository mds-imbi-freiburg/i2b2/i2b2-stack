# i2b2 Demo Data PostgreSQL DB

Linux bash scripts or Dockerfile to create a Docker postgreSQL database container with the i2b2 demo data and the configuration for the use in i2b2-net. This Setup will work inside the Docker i2b2-net network together with i2b2-wildfly (application-server) and i2b2-web (web-frontend) container.

## Quickstart with Data Installation from i2b2 Repository

This will generate a docker volume i2b2-pg-vol and install all data as described in https://community.i2b2.org/wiki/display/getstarted/Chapter+3.+Data+Installation into the i2b2-pg container. Data scripts are located in folder i2b2-data  which is a git submodule to i2b2 data repository https://github.com/i2b2/i2b2-data.git.

* Clone repo including its submodules:
  ```
  git clone --recurse-submodules REPOSITORY
  ```
  If you already checked out the repo, but without `recurse-submodules` use following command to get submodules:
  ```
  git submodule update --init --recursive
  ```
* To create docker images, load demo data and start i2b2 postgres container (i2b2-pg) call the script inside this directory with:
  ```
  sudo sh create_i2b2-pg.sh 2>&1|tee docker-build.log
  ```
  Examine the logfile `docker-build.log` and restart the associated i2b2-wildfly container.

## Even More Quicker-start from Data Backup 

This will load the data during initialisation on first start of the i2b2-pg container. It will take a few minutes until the i2b2-pg container have loaded the database dump from `backup_pg.sql.gz` gzip file.

* Build the PostgreSQL DB image with the backup dump
  ```
  docker build -t CONTAINER_REGISTRY/i2b2-demodata-pg .
  ```
* Run the container
  ```
  docker run -d -p 5432:5432 --net i2b2-net --mount source=i2b2-pg-vol,target=/var/lib/postgresql/data --name i2b2-pg CONTAINER_REGISTRY/i2b2-demodata-pg
  ```
  Check if the restore process is finished with:
  ```
  docker logs --tail 200 i2b2-pg 
  ```
  At the end of the restore process there should be a log message like: `2019-08-19 10:15:10.005 UTC [1] LOG:  database system is ready to accept connections`.  


## Create Data Installation from i2b2 Repository with pgTAP Unit Testing Framework

This will download, compile and install v1.1.0 of ***pgTAP*** on PostgreSQL 12.1 .

* Build the PostgreSQL DB image with the backup dump
  ```
  docker build -t CONTAINER_REGISTRY/i2b2-demodata-pg-pgtap -f Dockerfile_pgtap .
  ```

* Run the container
  ```
  docker run -d -p 5432:5432 --net i2b2-net --mount source=i2b2-pg-vol,target=/var/lib/postgresql/data --name i2b2-pg CONTAINER_REGISTRY/i2b2-demodata-pg-pgtap
  ```

## Miscellaneous

* Run a complete database backup
  ```
  docker exec -it i2b2-pg pg_dumpall -U postgres > backup_pg.sql
  ```
  Creates file `backup_pg.sql` file on the docker host.  
  To backup only the i2b2 database:  
  ```
  docker exec -it i2b2-pg pg_dump -U postgres -d i2b2 > backup_i2b2.sql
  ```
* To restore a database backup do:
  ```
  docker exec -it i2b2-pg pg_restore -U postgres -d i2b2  backup_i2b2.sql
  ```
  see also :  https://www.postgresql.org/docs/12/app-pgrestore.html 

