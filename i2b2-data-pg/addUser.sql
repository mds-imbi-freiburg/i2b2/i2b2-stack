-- Creates a new user <ldapname> and add it to the projects Demo and miracum
INSERT INTO i2b2pm.pm_user_data (user_id, full_name, "password", change_date, email, project_path, changeby_char, status_cd) VALUES(:'ldapuser', :'ldapuser', '9117d59a69dc49807671a51f10ab7f', CURRENT_TIMESTAMP, '', NULL, :'ldapuser', 'A');
INSERT INTO i2b2pm.pm_user_params (datatype_cd, user_id, param_name_cd, value, change_date, changeby_char, status_cd) VALUES('T', :'ldapuser', 'authentication_method', 'LDAP', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_user_params (datatype_cd, user_id, param_name_cd, value, change_date, changeby_char, status_cd) VALUES('T', :'ldapuser', 'search_base', 'ou=people,dc=example,dc=com', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_user_params (datatype_cd, user_id, param_name_cd, value, change_date, changeby_char, status_cd) VALUES('T', :'ldapuser', 'distinguished_name', 'uid=', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_user_params (datatype_cd, user_id, param_name_cd, value, change_date, changeby_char, status_cd) VALUES('T', :'ldapuser', 'security_authentication', 'simple', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_user_params (datatype_cd, user_id, param_name_cd, value, change_date, changeby_char, status_cd) VALUES('T', :'ldapuser', 'connection_url', 'ldaps://ldap.example.com:636', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'USER', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'DATA_PROT', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'DATA_OBFSC', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'DATA_AGG', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'DATA_LDS', CURRENT_TIMESTAMP, 'i2b2', 'A');
INSERT INTO i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, changeby_char, status_cd) VALUES('Demo', :'ldapuser', 'DATA_DEID', CURRENT_TIMESTAMP, 'i2b2', 'A');
