#!/bin/sh
# This script creates docker images, loads demo data and starts i2b2 containers
# call this script inside this directory with:
# sudo sh create_i2b2-pg.sh 2>&1|tee docker-build.log

if [ ! "$(docker network ls | grep i2b2-net)" ]; then
  echo "Creating i2b2-net network ..."
  docker network create i2b2-net
else
  echo "i2b2-net network exists."
fi

echo "Deleting existing i2b2-pg(-vol) containers, images and volumes."
docker rm -f i2b2-pg-vol; true
docker rmi CONTAINER_REGISTRY/i2b2-pg-vol; true
docker rm -f i2b2-pg; true
docker volume rm -f i2b2-pg-vol; true

docker run -d  -p 5432:5432 --net i2b2-net --mount source=i2b2-pg-vol,target=/var/lib/postgresql/data  -v `pwd`/i2b2-data/edu.harvard.i2b2.data/Release_1-7/:/data/ -v `pwd`/load_data.sh:/data/load_data.sh  --name i2b2-pg -e POSTGRES_PASSWORD=docker postgres:12.1

sleep 5
docker exec i2b2-pg /bin/bash /data/load_data.sh

docker run --rm --volumes-from i2b2-pg --name tmp_bk -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /var/lib/postgresql/data

#CREATE BACKUP CONTAINER embedding backup
docker run -d -v $(pwd):/backup --name i2b2-pg-vol ubuntu /bin/sh -c "cd / && tar xvf /backup/backup.tar"

#COMMIT
docker commit i2b2-pg-vol CONTAINER_REGISTRY/i2b2-pg-vol

echo "Please wait some minutes until all postgres transactions are finished. You might also have to restart the i2b2-wildfly container."
