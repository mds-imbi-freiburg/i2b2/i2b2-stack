# i2b2 Webclient

Docker buildfile to deploy the i2b webclient code and its configuration files to a httpd webserver. This Setup will work inside the Docker i2b2-net network together with i2b2-wildfly (application-server) and i2b2-pg (demo data-backend) container. This version runs the container as non root user *www-data* for security reasons. Therefore it has to use non-priviledged ports higher than 1024.

## Quickstart

* Clone repo including its submodules:
  ```
  git clone --recurse-submodules git@gitlab.com:mds-imbi-freiburg/i2b2/i2b2-stack.git
  ```
  If you already checked out the repo, but without `recurse-submodules` use following command to get submodules:
  ```
  git submodule update --init --recursive
  ```
* Build the local webclient image:
  ```
  docker build  -t DOCKER_REGISTRY/i2b2-web .
  ```
* Run the webclient  
  If not already done, create the network first:
  ```
  docker network create i2b2-net
  ```
  Start the web-frontend
  ```
  docker run -d  -p 443:8443 -p 80:8080 --net i2b2-net --name i2b2-web DOCKER_REGISTRY/i2b2-web
  ```
