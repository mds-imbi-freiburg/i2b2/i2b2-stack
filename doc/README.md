# i2b2 Configurable Stack

Documentation to the Freiburg University Medical Center flavored i2b2[^1] stack. 
This is an implementation of the i2b2 application stack in Docker[^2]. 
In its base version it is quick and easy to use and includes recent code and software versions. It has also the ability to compile the source code and generate the demo database from SQL files from the official i2b2 repositories.
In the current version this stack uses PostgreSQL[^3] 12.1 and wildfly 18.0.1. The documentation also describes how to include LDAP authentication in PostgreSQL and i2b2 web UI. Finally it takes a look at performance optimization.

Find code at [REPOSITORY](REPOSITORY)


## Quickstart with Standard Configuration

This will create and bring up i2b2-pg, i2b2-wildfly and i2b2-web container in its standard configuration with i2b2 demo data.

```bash 
docker-compose up -d
```

## Quickstart with an External Database
To connect to a postgres database on an external server.
* Edit the **ENV** variables in `i2b2-core-server/Dockerfile` :
```bash
ENV POSTGRES_SERVER=i2b2-pg
ENV POSTGRES_PORT=5432
ENV HIVE_PASSWORD=demouser
ENV CELL_PASSWORD=demouser
```
* Optional you might want to edit `i2b2-webclient/conf/i2b2_config_data.js` to set the proper i2b2 domain.
  set `domain:` and `name:` value:
```json
{
        urlProxy: "index.php",
        urlFramework: "js-i2b2/",
        //-------------------------------------------------------------------------------------------
        // THESE ARE ALL THE DOMAINS A USER CAN LOGIN TO
        lstDomains: [
                { domain: "i2b2mydomain",
                  name: "My Domain i2b2",
                  urlCellPM: "http://localhost/i2b2/services/PMService/",
                  allowAnalysis: true,
                     debug: true
                }
        ]
        //-------------------------------------------------------------------------------------------
}
```

* Open the client
Open the i2b2 webclient at http://localhost/webclient/.

possible logins:
        
- user:         i2b2   
  password:     demouser
- user:         demo    
  password:     demouser


## Quickstart with an Internal Database with LDAP PostgreSQL Users

* Configure LDAP Authentication for PostgreSQL

Configure your LDAP server details at the end of file: `i2b2-pg/conf/pg_hba.conf`.  
 
> **PostgreSQL documentation**: pg_hba.conf records are examined sequentially for each connection attempt, the order of the records is significant. 
> Typically, earlier records will have tight connection match parameters and weaker authentication methods, while later records will have looser match parameters 
> and stronger authentication methods. 

Example for LDAP connection:

```conf
...

# everyone from 10.1.1.x net can connect with ldap
host    all    all   10.1.1.0/24    ldap ldapserver=ldap.myserver.tld ldapbasedn="ou=people,dc=myserver,dc=tdl" ldapsearchattribute=uid ldaptls=0 ldapscheme=ldap ldapport=389

# from 10.1.2.x net user testuser can connect with ldap, everyone else with md5
host    all    testuser   10.1.2.0/24    ldap ldapserver=ldap.myserver.tld ldapbasedn="ou=people,dc=myserver,dc=tdl" ldapsearchattribute=uid ldaptls=0 ldapscheme=ldap ldapport=389
host    all    all        10.1.2.0/24    md5

# the user i2b2special from ip address 10.1.3.50, 10.1.3.51 and 10.1.3.52 cann connect database i2b2 with md5
host    i2b2          i2b2special      10.1.3.50/32          md5
host    i2b2          i2b2special      10.1.3.51/32          md5
host    i2b2          i2b2special      10.1.3.52/32          md5

...

```


* Start the docker stack with:
```bash
docker-compose  -f docker-compose.internal_db_ldap.yml up -d --build
```

### Administrate Users for LDAP Authentication

LDAP users do not exist in PostgreSQL. You have to create these users manually, with the same username (uid), in postgres. Example:

```
docker exec -it i2b2-pg psql -U postgres -d i2b2 -c "CREATE USER myUserName LOGIN INHERIT;"
```

#### LDAP Users for i2b2
* To create a LDAP user in **i2b2** use the `addUser.sql` script. 
  ```
  docker exec -i i2b2-pg psql -U postgres -d i2b2 -v ldapuser=myUserName < i2b2-pg/addUser.sql
  ```
  or to create a few:
  ```
  for u in mayer mair meier myer
  do
      docker exec -i i2b2pg psql -U postgres -d i2b2 -v ldapuser=$u < addUser.sql
  done
  ```
* Delete a user \<ldapuser\> with the `delUser.sql` script.
  ```
  docker exec -i i2b2pg psql -U postgres -d i2b2 -v ldapuser=<ldapuser> < delUser.sql
  ```
* Add admin flag to a user \<ldapuser\> with the `addAdmin.sql` script.
  ```
  docker exec -i i2b2pg psql -U postgres -d i2b2 -v ldapuser=<ldapuser> < addAdmin.sql
  ```

The variable *ldapuser* is passed to the *\*.sql* script with `-v ldapuser=MyUserName` and replaces `:'ldapuser'` inside the script.

## Prerequisites
To use the dockerized i2b2 stack make sure you have installed both [Docker Engine](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).



## Performance Optimization

To adapt configurations to your hardware environment, there are possibilities to adjust 
* Java settings in the i2b2-wildfly application container
* PostgreSQL settings in i2b2-pg

### Java settings in the i2b2-wildfly application container

In docker-compose.standard.yml is examplary entry to set `JAVA_OPTS` for the i2b2-wildfly application container via environment variable. 
You may adjust heap sizes to your needs. In the example below the maximum heap space is raised from 512 MB default to 1 GB by changing the value `-Xmx512m` to `-Xmx1g`.

```
...
  i2b2-wildfly:
    environment:
      - "JAVA_OPTS=-Xms64m -Xmx1g -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=1024m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"
    container_name: i2b2-wildfly 
...
```

### PostgreSQL settings in i2b2-pg
#### Optimization
At the end of file `i2b2-pg/conf/postgresql.conf` is an example on how to set optimized values to the i2b2-pg PostgreSQL database. To achive the settings `i2b2-pg/conf/postgresql.conf` has to be copied into the i2b2-pg container. And the container has to be restarted.
```
  docker cp i2b2-pg/conf/postgresql.conf i2b2-pg:/var/lib/postgresql/data/postgresql.conf
  docker restart i2b2-pg
```

or uncomment the according lines in `i2b2-pg/Dockerfile` if you use a setup where you build the i2b2-pg image locally. 

For optimal PostgreSQL settings see (https://pgtune.leopard.in.ua)

#### Logging
To enable debug logging of all queries in postgres run:

~~~bash
docker exec -it i2b2-pg bash -c "
echo  \"client_min_messages = debug1
log_line_prefix = '%m [%p] %q%u@%d '
log_statement = 'all'\" >> /var/lib/postgresql/data/postgresql.conf && pg_ctl reload"
~~~

#### Postgres Port
To change the port that postgres __internally__ listens on, run

~~~bash
docker exec -it i2b2-pg bash -c "echo \"port = 15432\" >> /var/lib/postgresql/data/postgresql.conf && pg_ctl reload"
~~~

## Footnotes
[^1]: [i2b2 Foundation on Github](https://github.com/i2b2)
[^2]: [Docker](https://www.docker.com)
[^3]: [PostgreSQL](https://www.postgresql.org/)
