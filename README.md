# i2b2-stack
In order to use our [i2b2 rls tool](https://gitlab.com/mds-imbi-freiburg/i2b2/i2b2rls) a selection of software is required in the i2b2 stack. 
Thus, based on the original i2b2 software, we provide this dockerized stack integrating additional components and Postgres 13.


[![pipeline status](https://gitlab.com/mds-imbi-freiburg/i2b2/i2b2-stack/badges/ci-build/pipeline.svg)](https://gitlab.com/mds-imbi-freiburg/i2b2/i2b2-stack/-/commits/ci-build) 

## getting started
This repo uses submodules, therefore clone it by calling
```bash
git clone --recurse-submodules git@gitlab.com:mds-imbi-freiburg/i2b2/i2b2-stack.git
```

If you already cloned it without the submodules just run the following command inside the project's folder;
```bash
git submodule update --init --recursive
```

## Quickstart with Standard Configuration

This will create and bring up i2b2-pg, i2b2-wildfly and i2b2-web container in its standard configuration with i2b2 demo data.


```bash
docker-compose -f docker-compose.build.yml up -d
```

__Note:__ Make sure to pull the latest images from the registry!


### Optional: Set i2b2 domain

You might want to edit `i2b2-webclient/conf/i2b2_config_data.js` to set the proper i2b2 domain.  
Set `domain:` and `name:` value:  
```js
{
        urlProxy: "index.php",
        urlFramework: "js-i2b2/",
        //-------------------------------------------------------------------------------------------
        // THESE ARE ALL THE DOMAINS A USER CAN LOGIN TO
        lstDomains: [
                { domain: "i2b2mydomain",
                  name: "My Domain i2b2",
                  urlCellPM: "http://localhost/i2b2/services/PMService/",
                  allowAnalysis: true,
                     debug: true
                }
        ]
        //-------------------------------------------------------------------------------------------
}
```
### Open the client
The client is now reachable at http://localhost/webclient/.

possible logins:
        
- user:         i2b2   
  password:     demouser
- user:         demo    
  password:     demouser


## Quickstart with an Internal Database with LDAP PostgreSQL Users

### Configure LDAP Authentication for PostgreSQL

Configure your LDAP server details at the end of file: `i2b2-pg/conf/pg_hba.conf`.   
Example for LDAP connection:

```
...

host    all    all   0.0.0.0/0    ldap ldapserver=ldap.example.com ldapbasedn="ou=people,dc=example,dc=com" ldapsearchattribute=uid ldaptls=0 ldapscheme=ldap ldapport=389

# comment to switch to LDAP Auth:
#host all all all trust

...
```

### Start the Stack
Start the docker stack with:
```bash
docker-compose  -f docker-compose.internal_db_ldap.yml up -d --build
```

## Build from scratch
To build all the images from source, use `docker-compose.build.yml`:

```bash
docker-compose -f docker-compose.build.yml up -d && docker-compose logs -f
```

This way you can modify the postgres [authentication config](i2b2-pg/conf/pg_hba.conf), 
include more [Datasources](i2b2-core-server/conf) for Wildfly,
set [your own domain](i2b2-webclient/conf/i2b2_config_data.js), and a lot more.

## Documentation
* [Documentation](./doc/)
* [User-Administration with LDAP Authentication](./doc/README.md#administrate-users-for-ldap-authentication)
* [Performance Optimization](./doc/README.md#performance-optimization)
